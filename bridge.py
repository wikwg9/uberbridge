#!/usr/bin/env python3
# -*- coding: utf-8 -*-


# ============== COMMON =====

import config
import logging

logging.basicConfig(filename=config.logfile, filemode='a', level=config.log_level, format='%(asctime)s - %(levelname)s - %(message)s')
logger = logging.getLogger(__name__)



# ============== FACEBOOK =====

from fbchat import Client
from fbchat.models import *
from wget import download
from os import unlink

class FBClient(Client):
    def onMessage(self, author_id, message_object, thread_id, thread_type, metadata, msg, **kwargs):
        """Called when the client is listening, and somebody sends a message.
        Args:
            mid: The message ID
            author_id: The ID of the author
            message: (deprecated. Use ``message_object.text`` instead)
            message_object (Message): The message (As a `Message` object)
            thread_id: Thread ID that the message was sent to. See :ref:`intro_threads`
            thread_type (ThreadType): Type of thread that the message was sent to. See :ref:`intro_threads`
            ts: The timestamp of the message
            metadata: Extra metadata about the message
            msg: A full set of the data received
        """

        if author_id == self.uid:
            logger.info("[FB] Read self sent message.")
            logger.debug("[FB] ===")
            logger.debug(metadata, msg)
            logger.debug("========\n")
            return

        self.markAsDelivered(thread_id, message_object.uid)
        self.markAsSeen()

        try:
            telegram_forward_dest = config.FB.telegram_destination[thread_id]
        except KeyError:
            logger.warning("[FB] Telegram bridge destination unknown.")
            return

        author_name = self.fetchUserInfo(author_id)[author_id].name

        if not message_object.text:
            logger.info("[FB] Skipping empty message.")
        elif message_object.text[0] == '!':
            logger.info("[FB] Skipping silenced message.")
        else:
            forward_text = author_name + ":\n" + message_object.text
            tg.send_text(forward_text, telegram_forward_dest)

        for attachment in message_object.attachments:
            if type(attachment) is FileAttachment:
                logger.info("[FB] Received a file.")
                tg.send_text(author_name + " sent a file...", telegram_forward_dest)
                tg.send_text(attachment.url, telegram_forward_dest)
            elif type(attachment) is ShareAttachment:
                #TODO: ShareAttachments are important, but not yet implemented
                logger.info("[FB] Received a ShareAttachment, which is not yet supported.")
                tg.send_text(author_name + " sent a share attachment.", telegram_forward_dest)
            elif type(attachment) is ImageAttachment:
                #TODO: send full-sized images as files
                logger.info("[FB] Received an image.")
                url = self.fetchImageUrl(attachment.uid)
                tg.send_image(url, telegram_forward_dest)
                tg.send_text(author_name + " sent an image...", telegram_forward_dest)
            elif type(attachment) is VideoAttachment:
                logger.info("[FB] Received a video.")
                tg.send_video(attachment.preview_url, telegram_forward_dest)
                tg.send_text(author_name + " sent a video...", telegram_forward_dest)
            elif type(attachment) is AudioAttachment:
                logger.info("[FB] Received an audio file.")
                tg.send_file(attachment.url, telegram_forward_dest)
                tg.send_text(author_name + " sent an audio file...", telegram_forward_dest)
            elif type(attachment) is LocationAttachment:
                logger.info("[FB] Received location.")
                attachment_forward_text = author_name + " sent location:\n"
                attachment_forward_text += "geo:" + attachment.latitude + "," + attachment.longitude + "\n"
                if attachment.address:
                    attachment_forward_text += "address: " + attachment.address + "\n"
                attachment_forward_text += "map link: " + attachment.url
                tg.send_text(attachment_forward_text, telegram_forward_dest)
            else:
                logger.error("[FB] Received some kind of unknown attachment!")

        if message_object.sticker:
            logger.info("[FB] Received a sticker.")
            tg.send_image(message_object.sticker.url, telegram_forward_dest)
            tg.send_text(author_name + " sent a sticker...", telegram_forward_dest)

        logger.info("[FB] {} from {}, {}. Thread type: {}".format(message_object, author_id, thread_id, thread_type.name))
        logger.debug("[FB] ===")
        logger.debug(metadata, msg)
        logger.debug("========\n")


    def onPollCreated(self, mid, poll, author_id, thread_id, thread_type, metadata, msg, **kwargs):
        """Called when the client is listening, and somebody creates a group poll.
        Args:
            mid: The action ID
            poll (Poll): Created poll
            author_id: The ID of the person who created the poll
            thread_id: Thread ID that the action was sent to. See :ref:`intro_threads`
            thread_type (ThreadType): Type of thread that the action was sent to. See :ref:`intro_threads`
            ts: A timestamp of the action
            metadata: Extra metadata about the action
            msg: A full set of the data received
        """

        if author_id == self.uid:
            logger.info("[FB] Read self sent message.")
            logger.debug("[FB] ===")
            logger.debug(metadata, msg)
            logger.debug("========\n")
            return

        self.markAsDelivered(thread_id, mid)
        self.markAsSeen()

        try:
            telegram_forward_dest = config.FB.telegram_destination[thread_id]
        except KeyError:
            logger.warning("[FB] Telegram bridge destination unknown.")
            return

        author_name = self.fetchUserInfo(author_id)[author_id].name

        forward_text = author_name + " created a poll:\n"
        forward_text += "Title: " + poll.title + "\n"
        forward_text += "Options:"

        for option in self.fetchPollOptions(poll.uid):
            forward_text += "\n - " + option.text

        tg.send_text(forward_text, telegram_forward_dest)

        logger.info("[FB] {} created poll {}, {}. Thread type: {}".format(author_id, poll, thread_id, thread_type.name))
        logger.debug("[FB] ===")
        logger.debug(metadata, msg)
        logger.debug("========\n")


    def wave_it(self, forward_dest):
        logger.info("[FB] Waving at %s", forward_dest)
        self.wave(wave_first=True, thread_id=forward_dest, thread_type=ThreadType.GROUP)


    def send_text(self, body, forward_dest):
        logger.info("[FB] Sending a text message to %s", forward_dest)
        self.send(Message(body), thread_id=forward_dest, thread_type=ThreadType.GROUP)


    def send_file(self, url, forward_dest):
        logger.info("[FB] Sending a remote file to %s", forward_dest)
        self.sendRemoteFiles([url], message=Message(text=''), thread_id=forward_dest, thread_type=ThreadType.GROUP)


    def send_image(self, url, forward_dest):
        logger.info("[FB] Downloading photo from %s", url)
        photo_filename = download(url)
        logger.info("[FB] Sending photo to %s", forward_dest)
        self.sendLocalImage(photo_filename, message=Message(text=''), thread_id=forward_dest, thread_type=ThreadType.GROUP)
        logger.info("[FB] Removing photo from %s", photo_filename)
        unlink(photo_filename)


    def send_video(self, url, forward_dest):
        self.send_file(url, forward_dest)



# ============== TELEGRAM =====

from telegram.ext import Updater, MessageHandler, Filters, CommandHandler

class TGClient():
    def __init__(self):
        self.updater = Updater(config.TG.bot_api_key)
        self.dp = self.updater.dispatcher

        self.dp.add_handler(MessageHandler(Filters.text, self.parse_text))
        self.dp.add_handler(MessageHandler(Filters.photo, self.parse_photo))
        self.dp.add_handler(MessageHandler(Filters.document | Filters.audio | Filters.video | Filters.voice, self.parse_document))
        self.dp.add_handler(MessageHandler(Filters.location, self.parse_location))
        self.dp.add_handler(MessageHandler(Filters.sticker, self.parse_sticker))

        self.dp.add_handler(CommandHandler("wave", self.wave_it))
        self.dp.add_handler(CommandHandler("logs", self.cmd_logs))

        self.dp.add_error_handler(self.error)
        self.updater.start_polling()
        logger.info("[TG] Logged in. Started polling.")


    def destinations(self, update):
        try:
            facebook_forward_dest = config.TG.facebook_destination[update.effective_chat.id]
        except KeyError:
            facebook_forward_dest = False
            logger.warning("[TG] Facebook bridge destination unknown.")
        return facebook_forward_dest


    def username(self, user):
        username = user.first_name
        if user.last_name is not None:
            username += " " + user.last_name
        return username


    def cmd_logs(self, bot, update):
        logger.info("[TG] user {} requested logs on chat {}".format(update.effective_user, update.message.chat_id))
        if update.effective_user.id not in config.TG.admins: return
        update.message.reply_text("user authorized\nsending logs...")
        bot.send_document(update.message.chat_id, document=open(config.logfile, "rb"))
        logger.info("[TG] logs sent")


    def parse_text(self, bot, update):
        if update.message.text[0] == '!':
            logger.info("[TG] Skipping silented message.")
            return

        facebook_forward_dest = self.destinations(update)
        if not facebook_forward_dest: return

        forward_body = self.username(update.message.from_user) + ":"

        if update.message.forward_from is not None:
            forward_body += "\n(FWD from " + self.username(update.message.forward_from) + ")"

        if update.message.reply_to_message:
            forward_body += "\n>>" + self.username(update.message.reply_to_message.from_user)
            for line in update.message.reply_to_message.text.split('\n'):
                forward_body += "\n>" + line

        forward_body += "\n"
        forward_body += update.message.text

        fb.send_text(forward_body, facebook_forward_dest)
        logger.info("[TG] Sent a text message.")


    def parse_photo(self, bot, update):
        facebook_forward_dest = self.destinations(update)
        if not facebook_forward_dest: return

        caption_body = self.username(update.message.from_user) + " sent an image..."
        if update.message.caption:
            caption_body += "\n" + update.message.caption

        image = update.message.photo[-1].get_file().file_path

        fb.send_text(caption_body, facebook_forward_dest)
        fb.send_image(image, facebook_forward_dest)
        logger.info("[TG] Sending photo %s", image)


    def parse_document(self, bot, update):
        facebook_forward_dest = self.destinations(update)
        if not facebook_forward_dest: return

        caption_body = self.username(update.message.from_user) + " sent a file..."
        if update.message.caption:
            caption_body += "\n" + update.message.caption

        document = update.message.effective_attachment.get_file()
        document = document.file_path

        fb.send_text(caption_body, facebook_forward_dest)
        fb.send_file(document, facebook_forward_dest)
        logger.info("[TG] Sending a file %s", document)


    def parse_location(self, bot, update):
        facebook_forward_dest = self.destinations(update)
        if not facebook_forward_dest: return

        forward_body = self.username(update.message.from_user) + " sent location:\n"
        if update.message.forward_from is not None:
            forward_body += "(FWD from " + self.username(update.message.forward_from) + ")\n"
        forward_body += "geo:" + str(update.message.location.latitude) + "," + str(update.message.location.longitude) + "\n"
        fb.send_text(forward_body, facebook_forward_dest)


    def parse_sticker(self, bot, update):
        facebook_forward_dest = self.destinations(update)
        if not facebook_forward_dest: return
        if not config.TG.facebook_forward_stickers: return

        caption_body = self.username(update.message.from_user) + " sent a sticker..."
        image = update.message.sticker.get_file().file_path

        if update.message.forward_from is not None:
            caption_body += "\n(FWD from " + self.username(update.message.forward_from) + ")"

        fb.send_text(caption_body, facebook_forward_dest)
        # apparently .webp is not an image on facebook, so it wouldn't send with send_image
        fb.send_file(image, facebook_forward_dest)
        logger.info("[TG] Sending a sticker %s", image)


    def wave_it(self, bot, update):
        facebook_forward_dest = self.destinations(update)
        if facebook_forward_dest:
            fb.send_wave(facebook_forward_dest)


    def error(self, bot, update, error):
        logger.error('[TG] "%s" caused error "%s"!', update, error)


    def send_text(self, body, forward_dest):
        logger.info("[TG] Sending a text message to %s", forward_dest)
        self.updater.bot.send_message(forward_dest, body)


    def send_file(self, url, forward_dest):
        logger.info("[TG] Sending a file to %s", forward_dest)
        self.updater.bot.send_document(forward_dest, url)


    def send_image(self, url, forward_dest):
        logger.info("[TG] Sending an image to %s", forward_dest)
        self.updater.bot.send_photo(forward_dest, url)


    def send_video(self, url, forward_dest):
        logger.info("[TG] Sending a video to %s", forward_dest)
        self.updater.bot.send_video(forward_dest, url)



# ============== BRIDGE =====

import signal
from importlib import reload

class Main():
    def __init__(self):
        signal.signal(signal.SIGUSR1, self.reload_config)
        self.facebook_side = FBClient(config.FB.login, config.FB.password, logging_level=config.log_level)
        self.telegram_side = TGClient()
        logger.info("All set.")

    def listen(self):
        self.facebook_side.listen()

    # define a function to catch signals and reload the config - reloading is done with USR1
    def reload_config(self, signal_number, frame):
        try:
            reload(config)
            logger.info("[config] reloaded config")
        except Exception as e:
            logger.warning("[config] couldn't reload config!")
            logger.info(e)


main = Main()
fb = main.facebook_side
tg = main.telegram_side

if __name__ == "__main__":
    main.listen()

